import urllib
import re
from bs4 import BeautifulSoup
from inspect import getsourcefile
import sys
import os
from urllib.parse import urlparse
import urllib.request
import mysql.connector
import yaml
from functions import sendMail

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

url = 'https://ispforum.cz/viewforum.php?f=2'
o = urlparse(url)

db_url = o.scheme+"://"+o.netloc+"/"

category = 'ISP Forum'

with urllib.request.urlopen(url) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")

l = soup.find_all("a", {"class" : "topictitle"} )
#l = k.find("div", {"class" : "forumbg"} ).find("ul", {"class" : "topiclist topics"} ).find_all("a", {"class" : "topictitle" } )

db_url_single = "";
db_title = "";
db_content = "";
db_price = "";

secret_user = conf['aws']['ispforum']['username']
secret_password = conf['aws']['ispforum']['password']
secret_database = conf['aws']['ispforum']['database']
secret_host = conf['aws']['ispforum']['host']
secret_port = conf['aws']['ispforum']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

for odkaz in l:
	db_url_single = db_url+odkaz.get('href')[2:]
	pattern = re.compile('https://ispforum.cz/viewtopic.php\?f=[0-9]+&t=[0-9]+', re.UNICODE)	
	db_url_single = pattern.match(db_url_single).group(0)
	title = odkaz.get_text()

	query1 = "SELECT * FROM ispforum.Items WHERE Item_URL = '"+db_url_single+"';"
	cursor.execute(query1)
	row = cursor.fetchall()

	if len(row) == 0:
		with urllib.request.urlopen(db_url_single) as response:
			html1 = response.read()
		
		soup1 = BeautifulSoup(html1, "html.parser")
		try:
			content = soup1.find("div", {"class":"post has-profile bg2"}).find("div",{"class":"content"})
		except:
			content = ""

		query = "INSERT INTO ispforum.Items (Item_URL) VALUES (%s)"
		try:
			content = "<html><body>"+str(db_url_single)+"<br><br>"+str(content)+"</body></html>"
			print(content)
			print(cursor._executed)
			sendMail(content,title)
		except:
			print("Problem pri odeslani mailu")

		cursor.execute(query, (db_url_single,))
		cnx.commit()

cursor.close()
cnx.close()
